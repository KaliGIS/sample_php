<?php
App::uses('IntegrationsController', 'Controller');

/**
 * Class PipedriveController
 *
 * @property Integration Integration
 * @property IntegrationConfigComponent IntegrationConfig
 * @property ClickToCallApp ClickToCallApp
 * @property Tag Tag
 * @property IntegrationExtInstance IntegrationExtInstance
 */
class PipedriveController extends IntegrationsController
{
    public $name = 'Integrations';
    public $uses = array('Company', 'ClickToCallApp', 'Integration', 'Tag', 'ContactsTag', 'IntegrationExtInstance');
    public $phActions = [
        'phadmin_pipedrive',
        'phadmin_pipedriveOauth',
    ];
    public $components = ['IntegrationConfig'];

    public function beforeFilter()
    {
        $this->partnerActions = [
            'phadmin_pipedrive',
            'phadmin_pipedriveOauth',
        ];
        parent::beforeFilter();

        $this->set('menuActiveGroup', MENU_SETTINGS);
        $this->set('menuActive', MENU_INTEGRATIONS);
        
        $this->Security->unlockedActions = [
            'pipedrive',
            'phadmin_pipedrive',
            'pipedriveOauth'
        ];
        if ($this->request->is('delete')) {
            $this->Auth->allow('pipedriveOauth');
        }
    }

    /**
     * PipedriveOauth set for phAdmin
     */
    public function phadmin_pipedriveOauth($fromCloudTalk = false, $integrationId = -1)
    {
        $this->Flash->warning(__('Oauth process can be done only by the customer'));
        return $this->redirect(['controller' => 'integrations', 'action' => 'index']);
    }

    /**
     * Oauth implementation for Pipedrive
     * @param $fromCloudTalk - true if this method is called from cloudtalk admin, false if this method is invoked from pipedrive app install page
     * @return CakeResponse|null
     *
     * @author martin.kalivoda
     */
    public function pipedriveOauth($fromCloudTalk = false, $integrationId = -1)
    {
        $isNotCsrfAttack = false;
        $isIntegrationSetupError = false;

        if ($this->request->is('delete')) {
            $basicAuthHeader = base64_encode(PIPEDRIVE_INTEGRATION_CLIENT_ID . ':' . PIPEDRIVE_INTEGRATION_CLIENT_SECRET);
            if ('Basic ' . $basicAuthHeader === $_SERVER['HTTP_AUTHORIZATION']) {
                // request is valid
                $integrations = $this->Integration->findByExtInstanceId(
                    $this->request->data['company_id'], // company_id is service key from Pipedrive
                    'pipedrive');
                if (empty($integrations)) {
                    CTLog::write('debug', 'No integration found for external instance id ' . $this->request->data['company_id']);
                    $this->response->statusCode(501);
                    $this->response->body('501');
                }
                CTLog::write('debug', 'Number of integrations to delete: ' . sizeof($integrations));
                foreach($integrations as $integration) { // all parallel integrations will be deleted
                    CTLog::write('debug', 'Integration ' . $integration['Integration']['id'] . ' is going to be deleted');
                    $this->delete($integration, $integration['Integration']['company_id']);
                }
            } else {
                $this->response->statusCode(404);
                $this->response->body('404');
            }
            return $this->response;
        }

        if (isset($this->request->query['error']) && $this->request->query['error'] == 'user_denied') {
            return $this->redirect(['controller' => 'integrations', 'action' => 'index']);
        }
        if (!isFeatureActive($this->company, 'integration_pipedrive_status')) {
            $this->Flash->error(__('Your company is not allowed to set %s integration. Please contact our support.', 'Pipedrive'));
            $this->redirect(['controller' => 'integrations', 'action' => 'index']);
        }
        $this->autoRender = false;

        $integration = null;
        if ($integrationId != -1 && !$this->Session->read('pipedriveUpdateOauthFlow')) { // existing instance setup
            $integration = $this->Integration->findById($integrationId, $this->company['Company']['id']);
        }
        if (!empty($integration)) {
            return $this->redirect(['controller' => 'pipedrive', 'action' => 'pipedriveSynchronizePersonsOrOrganizations', 0, $integration['Integration']['id']]);
        }

        $provider = new Daniti\OAuth2\Client\Provider\Pipedrive(array(
            'clientId' => PIPEDRIVE_INTEGRATION_CLIENT_ID,
            'clientSecret' => PIPEDRIVE_INTEGRATION_CLIENT_SECRET,
            'redirectUri' => Router::url(array('controller' => 'pipedrive', 'action' => 'pipedriveOauth'), true)
        ));

        // If we don't have an authorization code then get one
        if ($fromCloudTalk) {
            $authUrl = $provider->getAuthorizationUrl();
            $this->Session->write('pipedriveOauth2state', $provider->getState());
            $this->Session->write('pipedriveInitFromCt', true);
            $this->redirect($authUrl);
        }

        if ($this->Session->read('pipedriveInitFromCt')) {
            $this->Session->delete('pipedriveInitFromCt');
            // Check given state against previously stored one to mitigate CSRF attack
            if (empty($this->request->query['state']) || ($this->request->query['state'] !== $this->Session->read('pipedriveOauth2state'))) {
                $this->Session->delete('pipedriveOauth2state');
                $this->Flash->error(__('Error while authenticating. Cause: invalid state. Please try it again.'));
                $this->redirect(array('controller' => 'integrations', 'action' => 'index'));
            } else {
                $isNotCsrfAttack = true;
            }
        } else if (isset($this->request->query['code'])) {
            $isNotCsrfAttack = true;
        }

        if ($isNotCsrfAttack) {
            // Get access token using the authorization code grant
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $this->request->query['code']
            ]);
            try {
                $user = $provider->getResourceOwner($token)->toArray();
                
                $data['IntegrationExtInstance'] = [
                    'external_instance_id' => $user['data']['company_id'],
                    'instance_url' => 'https://' . $user['data']['company_domain'] . '.pipedrive.com',
                    'company_id' => $this->company['Company']['id'],
                    'refresh_token' => $token->getRefreshToken(),
                    'access_token' => $token->getToken(),
                    'updated' => formatDatetime(new DateTime(), $this->companyTimezone)
                ];

                if ($this->Session->read('pipedriveUpdateOauthFlow')) {
                    $integrationId = $this->Session->read('integrationId');
                    $this->Session->delete('pipedriveUpdateOauthFlow');
                    $this->Session->delete('integrationId');

                    $data['IntegrationExtInstance']['token_expiration'] = null;
                    $externalInstance = $this->IntegrationExtInstance->upsert($data); // verify if the new external instance id is the same as old one

                    if (!empty($externalInstance)) {
                        $integrations = $this->Integration->find('all', [
                            'conditions' => [
                                'Integration.integration_ext_instance_id' => $externalInstance['IntegrationExtInstance']['id'],
                                'Integration.company_id' => $this->company['Company']['id']
                            ],
                        ]); 

                        foreach($integrations as $integration) {
                            $updatedConfig = $integration['Integration']['config'];
                            $updatedConfig['oauthUpdated'] = 1;

                            $this->Integration->create(null);
                            $this->Integration->id = $integration['Integration']['id'];
                            $this->Integration->save(array(
                                'config' => $updatedConfig
                            ), false, array('config'));
                        }

                        $HttpSocket = new HttpSocket();
                        $request = array(
                            'method' => 'POST',
                            'uri' => array(
                                'scheme' => 'https',
                                'host' => 'hooks.slack.com',
                                'path' =>  IS_DEVELOPMENT_ENVIRONMENT ? 'services/TBMUGUCDP/BJU7P7WBA/KQ2IvYCkrvCyJu1R5f36BcMG' : 'services/TBMUGUCDP/BKL4S7GKT/6ju5TATpFlCntnVUQOhn5kTU',
                            ),
                            'version' => '1.1',
                            'body' => '{"text":"' . $this->company['Company']['name'] . ' - ' . $this->company['Company']['id'] . ' just reinstalled Pipedrive integration"}',
                            'header' => array(
                                'Content-type' => 'application/json'
                            ),
                        );
                        $result = $HttpSocket->request($request);

                        $firstTimeSetup = 0;
                        return $this->redirect([
                            'controller' => 'pipedrive',
                            'action' => 'pipedriveSynchronizePersonsOrOrganizations',
                            $firstTimeSetup,
                            $integrationId
                        ]);
                    } else {
                        $isIntegrationSetupError = true;
                    }
                } else {
                    $externalInstance = $this->IntegrationExtInstance->upsert($data);

                    if (!empty($externalInstance)) {
                        $data['Integration'] = [
                            'integration_ext_instance_id' => $externalInstance['IntegrationExtInstance']['id'],
                            'status' => -1,
                            'company_id' => $this->company['Company']['id']
                        ];
                        $pipedriveIntegration = $this->Integration->save($this->IntegrationConfig->preparePipedriveConfig($data), false);

                        if (!empty($pipedriveIntegration)) {
                            $firstTimeSetup = true;
                            return $this->redirect([
                                'controller' => 'pipedrive',
                                'action' => 'pipedriveSynchronizePersonsOrOrganizations',
                                $firstTimeSetup,
                                $pipedriveIntegration['Integration']['id']
                            ]);
                        } else {
                            $isIntegrationSetupError = true;
                        }
                    } else {
                        $isIntegrationSetupError = true;
                    }
                }

                if ($isIntegrationSetupError) {
                    $this->Flash->error(__('Error occurred during integration setup. Contact our support'));
                    $this->redirect(['controller' => 'integrations', 'action' => 'index']);
                }
            } catch (Exception $e) {
                CTLog::write('error', $e->getMessage());
                $this->Flash->error(__('Error occurred during integration setup. Contact our support'));
                $this->redirect(['controller' => 'integrations', 'action' => 'index']);
            }
        }
    }

    /**
     * Updates oauth tokens with new access rights
     * @author martin.kalivoda
     */
    public function pipedriveUpdateOauth($fromCloudTalk = false, $integrationId)
    {
        $this->Session->write('pipedriveUpdateOauthFlow', true);
        $this->Session->write('integrationId', $integrationId);

        $integration = $this->Integration->findById($integrationId, $this->logged['company_id']);
        if (!empty($integration)) {
            $this->pipedriveRevokeRefreshToken($integration['IntegrationExtInstance']['refresh_token']);
            return $this->redirect(['controller' => 'pipedrive', 'action' => 'pipedriveOauth', 1, $integrationId]);
        }
        
        $this->Flash->error(__('Something went wrong. Please try again.'));
        $this->redirect(['controller' => 'integrations', 'action' => 'index']);
    }

    /**
     * Pipedrive set for phAdmin
     */
    public function phadmin_pipedrive($firstTimeSetup = false, $integrationId = -1)
    {
        $this->pipedrive($firstTimeSetup, $integrationId);
        $this->render('pipedrive');
    }

    /**
     * Pipedrive integration settings
     * @return CakeResponse|null
     *
     * @author martin.kalivoda
     */
    public function pipedrive($firstTimeSetup = false, $integrationId = -1)
    {
        $this->Intercom->pushEvent('integration-visited', array(
            'integration-name' => INTEGRATION_NAME_PIPEDRIVE
        ));

        $this->set('menuActiveGroup', MENU_SETTINGS);
        $this->set('menuActive', MENU_INTEGRATIONS);
        $this->set('title_for_layout', __('Pipedrive Integration'));
        $this->set('firstTimeSetup', $firstTimeSetup);
        if (!isFeatureActive($this->company, 'integration_pipedrive_status')) {
            throw new NotFoundException();
        }

        $integration = null;
        if ($integrationId == -1) { // new instance setup
            return $this->redirect(['controller' => 'pipedrive', 'action' => 'pipedriveOauth', 1, -1]);
        } else { // existing instance setup
            $integration = $this->Integration->findById($integrationId, $this->company['Company']['id']);
        }

        if (empty($integration)) {
            return $this->redirect(['controller' => 'pipedrive', 'action' => 'pipedriveOauth', 1, -1]);
        }
        $this->set('integrationId', $integration['Integration']['id']);
        $this->set('integration', $integration['Integration']);

        $availableNumbers = $this->CallNumber->getByCompany($this->logged['company_id'], false, false);
        $this->set('availableNumbers', $availableNumbers);

        $availableAgents = $this->Agent->getAgentsForm($this->logged['company_id']);
        $this->set('availableAgents', $availableAgents);

        $pipedriveTag = null;
        if (!empty($integration)) {
            $pipedriveTag = $this->Tag->findFirstById($integration['Integration']['tag_id']);
        }
        $this->set('pipedriveTag', !empty($pipedriveTag) ? $pipedriveTag['Tag']['name'] : INTEGRATION_NAME_PIPEDRIVE);

        $activityTypes = $this->getActivityTypes($integration);
        $this->set('activityTypes', !empty($activityTypes) ? $activityTypes : array('call' => 'Call'));

        if (!empty($integration['Integration']['config']['mapContactsTo'])) {
            $this->set('mapContactsTo', $integration['Integration']['config']['mapContactsTo']);
        } else {
            $this->set('mapContactsTo', PIPEDRIVE_ENTITY_PERSON); // Default behaviour
        }

        $organizationFields = $this->getOrganizationFields($integration);
        $organizationFieldsForPhone = $organizationFields[0];
        $organizationFieldsForEmail = $organizationFields[1];
        $this->set('organizationFieldsForPhone', $organizationFieldsForPhone);
        $this->set('organizationFieldsForEmail', $organizationFieldsForEmail);
        if (empty($organizationFieldsForPhone) && empty($organizationFieldsForEmail)) { // something went wrong
            $this->set('mapContactsTo', PIPEDRIVE_ENTITY_PERSON); // Default behaviour
        }

        if (empty($integration['Integration']['config']['oauthUpdated'])) {
            $isOauthUpdated = false;
        } else {
            $isOauthUpdated = true;
        }

        if (new DateTime() > new DateTime(PIPEDRIVE_SCHEDULED_UPDATE_DATETIME) && !$isOauthUpdated) {
            $this->render('pipedriveUpdateOauth');
        }

        if ($this->request->is('post')) {
            if ($this->request->data['formSubmitBtn'] == 'delete') {
                $this->delete($integration, $this->company['Company']['id']);
                $this->redirect(['controller' => 'integrations', 'action' => 'index']);
            } else if ($this->request->data['formSubmitBtn'] == 'save') {
                $this->request->data['Integration'] = $this->IntegrationConfig->preparePipedriveConfig($this->request->data);
                $this->request->data['Integration']['id'] = null;
                if (!empty($integration['Integration']['id'])) {
                    $this->request->data['Integration']['id'] = $integration['Integration']['id'];
                }
                
                $this->request->data['Integration']['company_id'] = $this->logged['company_id'];
                $this->request->data['Integration']['name'] = INTEGRATION_NAME_PIPEDRIVE;
                $this->request->data['Integration']['config']['linkedCallNumberIds'] = $this->linkedNumbers($availableNumbers);
                $this->request->data['Integration']['config']['mapContactsTo'] = $integration['Integration']['config']['mapContactsTo'] ?? PIPEDRIVE_ENTITY_PERSON;
                $this->request->data['Integration']['config'] = $this->IntegrationConfig->updateConfig($this->request->data['Integration']['config']);

                $this->Integration->set($this->request->data['Integration']);
                if ($this->Integration->validates()) {
                    $tag = $this->Tag->createTag(
                        $this->request->data['Integration']['tag'],
                        $this->request->data['Integration']['company_id'],
                        INTEGRATION_NAME_PIPEDRIVE
                    );
                    if ($tag['success']) {
                        $this->request->data['Integration']['tag_id'] = $tag['id'];
                        $pipedriveIntegration = $this->Integration->save($this->request->data);
                        if ($this->request->data['Integration']['status'] <= 0) {
                            if (!empty($pipedriveIntegration)) {
                                $this->Flash->success(__('Integration settings was successfully saved.'));
                                $this->Intercom->pushCompanyEvent('integration-edited', array(
                                    'integration-name' => INTEGRATION_NAME_PIPEDRIVE
                                ));
                                $this->redirect(array('controller' => 'pipedrive', 'action' => 'pipedrive', 0, $pipedriveIntegration['Integration']['id']));
                            } else {
                                $this->request->data = $this->IntegrationConfig->preparePipedriveConfig($this->request->data);
                                $this->Flash->error(__('Error while saving the data. Please try it again.'));
                            }
                        } else {
                            if (!empty($pipedriveIntegration)) {
                                $this->Flash->success(__('Integration settings was successfully saved.'));
                                sendSQSMessageAsync([[
                                    'event' => SQS_MESSAGE_INTEGRATION_ENABLED,
                                    'company_id' => $this->company['Company']['id'],
                                    'integration_id' => $this->Integration->id,
                                ]]);

                                $this->Intercom->pushCompanyEvent('integration-edited', array(
                                    'integration-name' => INTEGRATION_NAME_PIPEDRIVE
                                ));
                                $this->redirect(array('controller' => 'pipedrive', 'action' => 'pipedrive', 0, $pipedriveIntegration['Integration']['id']));
                            } else {
                                $this->request->data = $this->IntegrationConfig->preparePipedriveConfig($this->request->data);
                                $this->Flash->error(__('Error while saving the data. Please try it again.'));
                            }
                        }
                    } else {
                        $this->Integration->invalidate('tag', $tag['msg']);
                        $this->request->data = $this->IntegrationConfig->preparePipedriveConfig($this->request->data);
                        $this->Flash->error(__('Error while saving the data. Please try it again.'));
                    }
                } else {
                    $this->request->data = $this->IntegrationConfig->preparePipedriveConfig($integration);
                }
            }
        } else {
            $this->request->data = $this->IntegrationConfig->preparePipedriveConfig($integration);
        }
    }

    /**
     * Revokes Pipedrive refresh token, a.k.a. uninstalls CloudTalk app at Pipedrive
     * 
     * @author martin.kalivoda
     */
    public function pipedriveRevokeRefreshToken($refreshToken) : bool {
        $pipedriveCredsEncoded = base64_encode(PIPEDRIVE_INTEGRATION_CLIENT_ID . ':' . PIPEDRIVE_INTEGRATION_CLIENT_SECRET);
        
        $curlObj = curl_init();
        $curlOptions = [
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_URL => 'https://oauth.pipedrive.com/oauth/revoke',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
                'Authorization: Basic ' . $pipedriveCredsEncoded 
            ),
            CURLOPT_HEADER => true,
            CURLOPT_POSTFIELDS => 'token=' . $refreshToken . '&token_type_hint=refresh_token'
        ];
        curl_setopt_array($curlObj, $curlOptions);
        $curlResponse = curl_exec($curlObj);
        curl_close($curlObj);
        
        if (!empty($curlResponse)) {
            $headers = AppComponent::getHeadersFromCurlResponse($curlResponse);
            if (strpos($headers['http_code'], '200') !== false) {
                CTLog::write('debug', 'Pipedrive token revokation, company ID ' . $this->logged['company_id'] . ': Success!');
            } else {
                CTLog::write('error', 'Pipedrive token revokation, company ID ' . $this->logged['company_id'] . ': Another status code returned as 200', [], ['headers' => $headers]);
            }
        } else {
            CTLog::write('error', 'Pipedrive token revokation, company ID ' . $this->logged['company_id'] . ': Empty $curlResponse returned');
        }

        return true;
    }

    /**
     * Gets new access token by refresh token provided.
     * 
     * @author martin.kalivoda
     */
    private function refreshAccessToken($integration) : array {
        $tokenExpirationTimestamp = null;
        $newAccessToken = null;

        $pipedriveCredsEncoded = base64_encode(PIPEDRIVE_INTEGRATION_CLIENT_ID . ':' . PIPEDRIVE_INTEGRATION_CLIENT_SECRET);
        $curlObj = curl_init();
        $curlOptions = [
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_URL => 'https://oauth.pipedrive.com/oauth/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
                'Authorization: Basic ' . $pipedriveCredsEncoded 
            ),
            CURLOPT_HEADER => true,
            CURLOPT_POSTFIELDS => 'grant_type=refresh_token&refresh_token=' . $integration['IntegrationExtInstance']['refresh_token']
        ];
        curl_setopt_array($curlObj, $curlOptions);
        $curlResponse = curl_exec($curlObj);
        
        if (!empty($curlResponse)) {
            $header_size = curl_getinfo($curlObj, CURLINFO_HEADER_SIZE);
            $body = json_decode(substr($curlResponse, $header_size), true);
            $headers = AppComponent::getHeadersFromCurlResponse($curlResponse);
            if (strpos($headers['http_code'], '200') !== false) {
                $dateTime = new DateTime();
                $tokenExpirationTimestamp = ($body['expires_in'] + $dateTime->getTimestamp()) * 1000; // * 1000 because of converting seconds to miliseconds
                $newAccessToken = $body['access_token'];

                $this->IntegrationExtInstance->create(null);
                $this->IntegrationExtInstance->id = $integration['IntegrationExtInstance']['id'];
                $this->IntegrationExtInstance->save(array(
                    'token_expiration' => $tokenExpirationTimestamp,
                    'access_token' => $newAccessToken            
                ), false, array('token_expiration', 'access_token'));
            } else {
                CTLog::write('error', 'Pipedrive refresh access token, company ID ' . $this->logged['company_id'] . ': Another status code returned as 200', [], ['headers' => $headers]);
            }
        } else {
            CTLog::write('error', 'Pipedrive refresh access token, company ID ' . $this->logged['company_id'] . ': Empty $curlResponse returned');
        }

        curl_close($curlObj);

        return array(
            'tokenExpirationTimestamp' => $tokenExpirationTimestamp,
            'newAccessToken' => $newAccessToken
        );
    }

    /**
     * Gets Pipedrive activity types from API
     * 
     * @author martin.kalivoda
     */
    public function getActivityTypes($integration) : array {
        $dateTime = new DateTime();
        $activityTypes = array();
        
        if (($dateTime->getTimestamp() * 1000) > ($integration['IntegrationExtInstance']['token_expiration'] - 60000)) { // 60000 miliseconds
            $tokenData = $this->refreshAccessToken($integration);
            $integration['IntegrationExtInstance']['access_token'] = $tokenData['newAccessToken'];
            $integration['IntegrationExtInstance']['token_expiration'] = $tokenData['tokenExpirationTimestamp'];
        }

        $curlObj = curl_init();
        $curlOptions = [
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_URL => 'https://api-proxy.pipedrive.com/activityTypes',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $integration['IntegrationExtInstance']['access_token'],
                'Accept: application/json', 
                'Content-Type: application/json'
            ),
            CURLOPT_HEADER => true
        ];
        curl_setopt_array($curlObj, $curlOptions);
        $curlResponse = curl_exec($curlObj);
        
        if (!empty($curlResponse)) {
            $header_size = curl_getinfo($curlObj, CURLINFO_HEADER_SIZE);
            $body = substr($curlResponse, $header_size);
            $headers = AppComponent::getHeadersFromCurlResponse($curlResponse);
            if (strpos($headers['http_code'], '200') !== false) {
                $activityTypesReturned = json_decode($body, true)['data'];
                foreach ($activityTypesReturned as $activityTypeReturned) {
                    if ($activityTypeReturned['active_flag']) {
                        $activityTypes[$activityTypeReturned['key_string']] = $activityTypeReturned['name'];
                    }
                }
            } else {
                CTLog::write('error', 'Pipedrive get activity types, company ID ' . $this->logged['company_id'] . ': Another status code returned as 200', [], ['headers' => $headers]);
            }
        } else {
            CTLog::write('error', 'Pipedrive get activity types, company ID ' . $this->logged['company_id'] . ': Empty $curlResponse returned');
        }

        curl_close($curlObj);

        return $activityTypes;
    }

    /**
     * First setup page where the fundamental synchronization mapping logic can be chosen.
     * CloudTalk contacts can be mapped either to Pipedrive persons or Pipedrive organizations.
     * 
     * @author martin.kalivoda
     */
    public function pipedriveSynchronizePersonsOrOrganizations($firstTimeSetup = true, $integrationId = -1) : void {
        $this->set('menuActiveGroup', MENU_SETTINGS);
        $this->set('menuActive', MENU_INTEGRATIONS);
        
        if (!isFeatureActive($this->company, 'integration_pipedrive_status')) {
            $this->Flash->error(__('Your company is not allowed to set %s integration. Please contact our support.', 'Pipedrive'));
            $this->redirect(['controller' => 'integrations', 'action' => 'index']);
        }       

        if (!$firstTimeSetup) {
            $this->redirect(['controller' => 'pipedrive', 'action' => 'pipedrive']);
        }

        $this->set('integrationId', $integrationId);

        if ($this->request->is('post')) {
            // save the integration
            $defaultConfig = $this->IntegrationConfig->preparePipedriveConfig([]);
            $defaultConfig['config']['mapContactsTo'] = $this->request->data['Integration']['config']['mapContactsTo'];
            
            $this->Integration->create(null);
            $this->Integration->id = $integrationId;
            $pipedriveIntegration = $this->Integration->save(array(
                'config' => $defaultConfig['config']
            ), false, array('config'));

            if (!empty($pipedriveIntegration)) {
                $this->redirect(['controller' => 'pipedrive', 'action' => 'pipedrive', $firstTimeSetup, $integrationId]);
            } else {
                $this->Flash->error(__('Error while saving the data. Please try it again.'));
                $this->redirect(['controller' => 'integrations', 'action' => 'index']);
            }
        }
    }

    /**
     * Gets Pipedrive organization fields from API
     * 
     * @uathor martin.kalivoda
     */
    public function getOrganizationFields($integration) : array {
        $dateTime = new DateTime();
        $organizationFieldsForPhone = array();
        $organizationFieldsForEmail = array();
        
        if (($dateTime->getTimestamp() * 1000) > ($integration['IntegrationExtInstance']['token_expiration'] - 60000)) { // 60000 miliseconds
            $tokenData = $this->refreshAccessToken($integration);
            $integration['IntegrationExtInstance']['access_token'] = $tokenData['newAccessToken'];
            $integration['IntegrationExtInstance']['token_expiration'] = $tokenData['tokenExpirationTimestamp'];
        }

        $curlObj = curl_init();
        $curlOptions = [
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_URL => 'https://api-proxy.pipedrive.com/organizationFields',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $integration['IntegrationExtInstance']['access_token'],
                'Accept: application/json', 
                'Content-Type: application/json'
            ),
            CURLOPT_HEADER => true
        ];
        curl_setopt_array($curlObj, $curlOptions);
        $curlResponse = curl_exec($curlObj);
        
        if (!empty($curlResponse)) {
            $header_size = curl_getinfo($curlObj, CURLINFO_HEADER_SIZE);
            $body = substr($curlResponse, $header_size);
            $headers = AppComponent::getHeadersFromCurlResponse($curlResponse);
            if (strpos($headers['http_code'], '200') !== false) {
                $organizationFieldsReturned = json_decode($body, true)['data'];
                foreach ($organizationFieldsReturned as $organizationFieldReturned) {
                    if ($organizationFieldReturned['active_flag']) {
                        if ($organizationFieldReturned['field_type'] == PIPEDRIVE_ORGANIZATION_FIELD_TYPE_PHONE) {
                            $organizationFieldsForPhone[$organizationFieldReturned['key']] = $organizationFieldReturned['name'];
                        }
                        if (
                            $organizationFieldReturned['field_type'] == PIPEDRIVE_ORGANIZATION_FIELD_TYPE_EMAIL_1 ||
                            $organizationFieldReturned['field_type'] == PIPEDRIVE_ORGANIZATION_FIELD_TYPE_EMAIL_2
                        ) {
                            $organizationFieldsForEmail[$organizationFieldReturned['key']] = $organizationFieldReturned['name'];
                        }
                    }
                }
            } else {
                CTLog::write('error', 'Pipedrive get organization fields, company ID ' . $this->logged['company_id'] . ': Another status code returned as 200', [], ['headers' => $headers]);
            }
        } else {
            CTLog::write('error', 'Pipedrive get organization fields, company ID ' . $this->logged['company_id'] . ': Empty $curlResponse returned');
        }

        curl_close($curlObj);

        return [$organizationFieldsForPhone, $organizationFieldsForEmail];
    }
}