<?php
App::uses('IntegrationsController', 'Controller');

/**
 * Class SalesforceController
 *
 * @property Integration Integration
 * @property IntegrationConfigComponent IntegrationConfig
 * @property ClickToCallApp ClickToCallApp
 * @property Tag Tag
 * @property IntegrationExtInstance IntegrationExtInstance
 */
class SalesforceController extends IntegrationsController
{
    public $name = 'Integrations';
    public $uses = array('Company', 'ClickToCallApp', 'Integration', 'Tag', 'ContactsTag', 'IntegrationExtInstance');
    public $phActions = [
        'phadmin_salesforce',
        'phadmin_salesforceOauth',
        'phadmin_salesforcePackageInstall',
        'phadmin_salesforceBeforeOauth'
    ];
    public $components = ['IntegrationConfig'];

    public function beforeFilter()
    {
        $this->partnerActions = [
            'phadmin_salesforce',
            'phadmin_salesforceOauth',
            'phadmin_salesforcePackageInstall',
            'phadmin_salesforceBeforeOauth'
        ];
        parent::beforeFilter();

        $this->set('menuActiveGroup', MENU_SETTINGS);
        $this->set('menuActive', MENU_INTEGRATIONS);
        $this->Security->unlockedActions = [
            'salesforce',
            'phadmin_salesforce',
            'phadmin_salesforcePackageInstall',
            'phadmin_salesforceBeforeOauth'
        ];
    }

    /**
     * SalesforceOauth set for phAdmin
     */
    public function phadmin_salesforceOauth($fromCloudTalk = false, $integrationId = -1)
    {
        $this->Flash->warning(__('Oauth process can be done only by the customer'));
        return $this->redirect(['controller' => 'integrations', 'action' => 'index']);
    }

    /**
     * Oauth implementation for Salesforce
     * @return CakeResponse|null
     *
     * @author martin.kalivoda
     */
    public function salesforceOauth($fromCloudTalk = false, $integrationId = -1) // todo implement from/outside cloudtalk scenario
    {
        $isIntegrationSetupError = false;

        if (!isFeatureActive($this->company, 'integration_salesforce_status')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;

        $integration = null;
        if ($integrationId != -1) { // existing instance setup
            debug('integration id set');
            $integration = $this->Integration->findById($integrationId, $this->company['Company']['id']);
        }
        if (!empty($integration)) {
            debug('integration is not empty');
            return $this->redirect(['controller' => 'salesforce', 'action' => 'salesforce', 0, $integration['Integration']['id']]);
        }

        $provider = new Stevenmaguire\OAuth2\Client\Provider\Salesforce([
            'clientId'          => SALESFORCE_INTEGRATION_CLIENT_ID,
            'clientSecret'      => SALESFORCE_INTEGRATION_CLIENT_SECRET,
            'redirectUri'       => Router::url(array('controller' => 'salesforce', 'action' => 'salesforceOauth'), true)
        ]);

        if (!isset($this->request->query['code'])) {
            // If we don't have an authorization code then get one
            $authUrl = $provider->getAuthorizationUrl();
            $this->Session->write('oauth2state', $provider->getState());
            $this->redirect($authUrl);

            // Check given state against previously stored one to mitigate CSRF attack
        } elseif (empty($this->request->query['state']) || ($this->request->query['state'] !== $this->Session->read('oauth2state'))) {
            $this->Session->delete('oauth2state');
            CTLog::write('error', 'Salesforce oauth process error: invalid state. Company id: ' . $this->company['Company']['id']);
            $this->Flash->error(__('Error occurred during integration setup. Contact our support'));
            $this->redirect(['controller' => 'integrations', 'action' => 'index']);
        } else {
            // Get access token using the authorization code grant
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $this->request->query['code']
            ]);
            try {
                $user = $provider->getResourceOwner($token)->toArray();

                $data['IntegrationExtInstance'] = [
                    'external_instance_id' => $user['organization_id'],
                    'instance_url' => $token->getInstanceUrl(),
                    'company_id' => $this->company['Company']['id'],
                    'refresh_token' => $token->getRefreshToken(),
                    'access_token' => $token->getToken(),
                    'updated' => formatDatetime(new DateTime(), $this->companyTimezone)
                ];
                $externalInstance = $this->IntegrationExtInstance->upsert($data);

                if (!empty($externalInstance)) {
                    $data['Integration'] = [
                        'integration_ext_instance_id' => $externalInstance['IntegrationExtInstance']['id'],
                        'status' => -1,
                        'company_id' => $this->company['Company']['id'],
                    ];
                    $salesforceIntegration = $this->Integration->save($this->IntegrationConfig->prepareSalesforceConfig($data), false);

                    if (!empty($salesforceIntegration)) {
                        $firstTimeSetup = true;
                        return $this->redirect([
                            'controller' => 'salesforce',
                            'action' => 'salesforce',
                            $firstTimeSetup,
                            $salesforceIntegration['Integration']['id']
                        ]);
                    } else {
                        $isIntegrationSetupError = true;
                    }
                } else {
                    $isIntegrationSetupError = true;
                }

                if ($isIntegrationSetupError) {
                    $this->Flash->error(__('Error occurred during integration setup. Contact our support'));
                    $this->redirect(['controller' => 'integrations', 'action' => 'index']);
                }
            } catch (Exception $e) {
                CTLog::write('error', $e->getMessage());
                $this->Flash->error(__('Error occurred during integration setup. Contact our support'));
                $this->redirect(['controller' => 'integrations', 'action' => 'index']);
            }
        }
    }

    /**
     * Salesforce set for phAdmin
     * @param bool $firstTimeSetup
     * @param int $integrationId
     */
    public function phadmin_salesforce($firstTimeSetup = false, $integrationId = -1)
    {
        $this->salesforce($firstTimeSetup, $integrationId);
        $this->render('salesforce');
    }

    /**
     * Salesforce integration settings
     * @param bool $firstTimeSetup
     * @param int $integrationId
     * @return CakeResponse|null
     *
     * @author martin.kalivoda
     */
    public function salesforce($firstTimeSetup = false, $integrationId = -1)
    {
        $this->Intercom->pushEvent('integration-visited', array(
            'integration-name' => INTEGRATION_NAME_SALESFORCE
        ));

        $this->set('menuActiveGroup', MENU_SETTINGS);
        $this->set('menuActive', MENU_INTEGRATIONS);
        $this->set('title_for_layout', __('Salesforce Integration'));
        $this->set('firstTimeSetup', $firstTimeSetup);
        if (!isFeatureActive($this->company, 'integration_salesforce_status')) {
            throw new NotFoundException();
        }

        $integration = null;
        if ($integrationId == -1) {
            return $this->redirect(['controller' => 'salesforce', 'action' => 'salesforceOauth', 1, -1]);
        } else { // existing instance setup
            $integration = $this->Integration->findById($integrationId, $this->company['Company']['id']);
        }

        if (empty($integration)) {
            return $this->redirect(['controller' => 'salesforce', 'action' => 'salesforceOauth', 1, -1]);
        }
        $this->set('integrationId', $integration['Integration']['id']);
        $this->set('integration', $integration['Integration']);

        $availableNumbers = $this->CallNumber->getByCompany($this->logged['company_id'], false, false);
        $this->set('availableNumbers', $availableNumbers);

        $salesforceTag = null;
        if (!empty($integration)) {
            $salesforceTag = $this->Tag->findFirstById($integration['Integration']['tag_id']);
        }
        $this->set('salesforceTag', !empty($salesforceTag) ? $salesforceTag['Tag']['name'] : INTEGRATION_NAME_SALESFORCE);

        if ($this->request->is('post')) {
            if ($this->request->data['formSubmitBtn'] == 'delete') {
                $this->delete($integration, $this->company['Company']['id']);
                $this->redirect(['controller' => 'integrations', 'action' => 'index']);
            } else if ($this->request->data['formSubmitBtn'] == 'save') {
                $this->request->data['Integration']['id'] = null;
                if (!empty($integration['Integration']['id'])) {
                    $this->request->data['Integration']['id'] = $integration['Integration']['id'];
                }

                $this->request->data['Integration']['company_id'] = $this->logged['company_id'];
                $this->request->data['Integration']['name'] = INTEGRATION_NAME_SALESFORCE;
                $this->request->data['Integration']['config']['linkedCallNumberIds'] = $this->linkedNumbers($availableNumbers);
                $this->request->data['Integration']['config'] = $this->IntegrationConfig->updateSalesforceConfig($this->request->data['Integration']['config']);

                $this->Integration->set($this->request->data['Integration']);
                if ($this->Integration->validates()) {
                    $tag = $this->Tag->createTag(
                        $this->request->data['Integration']['tag'],
                        $this->request->data['Integration']['company_id'],
                        INTEGRATION_NAME_SALESFORCE
                    );
                    if ($tag['success']) {
                        $this->request->data['Integration']['tag_id'] = $tag['id'];
                        $salesforceIntegration = $this->Integration->save($this->request->data);
                        if ($this->request->data['Integration']['status'] <= 0) {
                            if (!empty($salesforceIntegration)) {
                                $this->Flash->success(__('Integration settings was successfully saved.'));
                                $this->Intercom->pushCompanyEvent('integration-edited', array(
                                    'integration-name' => INTEGRATION_NAME_SALESFORCE
                                ));
                                $this->redirect(array('controller' => 'salesforce', 'action' => 'salesforce', 0, $salesforceIntegration['Integration']['id']));
                            } else {
                                $this->request->data = $this->IntegrationConfig->prepareSalesforceConfig($this->request->data);
                                $this->Flash->error(__('Error while saving the data. Please try it again.'));
                            }
                        } else {
                            if (!empty($salesforceIntegration)) {
                                $this->Flash->success(__('Integration settings was successfully saved.'));
                                sendSQSMessageAsync([[
                                    'event' => SQS_MESSAGE_INTEGRATION_ENABLED,
                                    'company_id' => $this->company['Company']['id'],
                                    'integration_id' => $this->Integration->id,
                                ]]);

                                $this->Intercom->pushCompanyEvent('integration-edited', array(
                                    'integration-name' => INTEGRATION_NAME_SALESFORCE
                                ));
                                $this->redirect(array('controller' => 'salesforce', 'action' => 'salesforce', 0, $salesforceIntegration['Integration']['id']));
                            } else {
                                $this->request->data = $this->IntegrationConfig->prepareSalesforceConfig($this->request->data);
                                $this->Flash->error(__('Error while saving the data. Please try it again.'));
                            }
                        }
                    } else {
                        $this->Integration->invalidate('tag', $tag['msg']);
                        $this->request->data = $this->IntegrationConfig->prepareSalesforceConfig($this->request->data);
                        $this->Flash->error(__('Error while saving the data. Please try it again.'));
                    }
                } else {
                    $this->request->data = $this->IntegrationConfig->prepareSalesforceConfig($this->request->data);
                }
            }
        } else {
            $this->request->data = $this->IntegrationConfig->prepareSalesforceConfig($integration);
        }
    }

    /**
     * Salesforce package install set for phAdmin
     */
    public function phadmin_salesforcePackageInstall() : void {
        $this->salesforcePackageInstall();
        $this->render('salesforce_package_install');
    }

    /**
     * Renders page before Salesforce package install with instructions and redirect button
     * to install Salesforce package.
     * 
     * @author martin.kalivoda
     */
    public function salesforcePackageInstall() : void {
        
    }

    /**
     * Salesforce page before oauth set for phAdmin
     */
    public function phadmin_salesforceBeforeOauth() : void {
        $this->salesforceBeforeOauth();
        $this->render('salesforce_before_oauth');
    }

    /**
     * Renders page before Salesforce oauth authentication process.
     * 
     * @author martin.kalivoda
     */
    public function salesforceBeforeOauth() : void {

    }
}